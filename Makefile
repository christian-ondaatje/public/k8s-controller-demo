##
# ssh-control
#
# @file
# @version 0.1

export NAMESPACE=ssh-control

install | update:
	 # deploy controller
	kubectl create configmap ssh-controller-configmap \
		--from-file=./controller/ssh-controller.sh \
		-n ${NAMESPACE} \
		-o yaml \
		--dry-run=client | kubectl apply -f -

	 # create webserver configmap (hack)
	kubectl create configmap webserver-configmap \
		--from-file=./webserver/webserver.go \
		-n ${NAMESPACE} \
		-o yaml \
		--dry-run=client | kubectl apply -f -

deploy:
	kubectl -n ${NAMESPACE} apply -f ./controller/ssh-controller.yml
	kubectl -n ${NAMESPACE} apply -f ./webserver/deployment.yml
	kubectl -n ${NAMESPACE} apply -f ./webserver/service.yml

undeploy:
	kubectl -n ${NAMESPACE} delete -f ./controller/ssh-controller.yml
	kubectl -n ${NAMESPACE} delete -f ./webserver/deployment.yml
	kubectl -n ${NAMESPACE} delete -f ./webserver/service.yml

redeploy: undeploy install deploy

demo:
	kubectl -n ${NAMESPACE} apply -f ./webserver/example-ssh-configmap.yml

json:
	 # kubectl -n ${NAMESPACE} apply -f ./example-ssh-configmap.yml --dry-run=client -o json > out.json
	 # kubectl -n ${NAMESPACE} apply -f ./config-watcher-controller.yml --dry-run=client -o json > out.json
	kubectl -n ${NAMESPACE} apply -f ./web-app.yml --dry-run=client -o json > out.json
	 # kubectl -n ${NAMESPACE} apply -f ./ssh-dp.yml --dry-run=client -o json > out.json
	 # kubectl -n ${NAMESPACE} apply -f ./ssh-controller.yml --dry-run=client -o json > out.json

jsonmap:
	kubectl create configmap ssh-config-user1 \
		--from-file=./webserver/example-ssh-configmap.yml \
		-n ${NAMESPACE} \
		-o json \
		--dry-run=client > out1.json

clean:
	kubectl delete ns ssh-control
	kubectl create ns ssh-control
	 # kubectl -n ${NAMESPACE} delete -f ./ssh-controller.yml
	 # kubectl -n ${NAMESPACE} delete -f ./example-ssh-configmap.yml
	 # kubectl -n ${NAMESPACE} delete configmap ssh-controller-configmap

# end
