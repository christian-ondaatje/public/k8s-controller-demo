#!/usr/bin/env sh

# Controller script which watches configmaps and evaluates ssh keys within,
# to make ssh pods with pytorch image

# hard-coded for simplicity of demo - see k8spatterns reference code for dynamic namespacing
namespace="ssh-control"

# API URL setup. Requires an ambassador API proxy running side-by-side on localhost
base=http://localhost:8001
ns=namespaces/$namespace

k8s_deployment_url=$base/apis/apps/v1/$ns/deployments
k8s_configmap_url=$base/api/v1/$ns/configmaps
k8s_service_url=$base/api/v1/$ns/services

start_event_loop() {
  # Watch the K8s API on events on service objects
  echo "::: Starting to wait for events"

  # Event loop listening for changes in config maps
  curl -N -s $base/api/v1/${ns}/configmaps?watch=true | while read -r event
  do
    # Sanitize new lines
    event=$(echo "$event" | tr '\r\n' ' ')

    # Event type & name
    local type=$(echo "$event" | jq -r .type)
    local config_map=$(echo "$event" | jq -r .object.metadata.name)

    local user=$(echo "$event" | jq -r '.object.data.user')
    local pubkey=$(echo "$event" | jq -r '.object.data.pubkey')
    local nodeport=$(echo "$event" | jq -r '.object.data.nodeport')
    echo $user
    echo $pubkey
    echo $nodeport

    # this logic would be more robust in a production controller of course
    # for example, we could add a data field 'ssh-box: true' to our configmap idiom
    # and check for it before doing any automated action.
    # or better still, we would create a CRD and follow the Operator Pattern
    if [ "$pubkey" != "null" ]; then
        # we have got a pubkey, let's create the ssh pod for this user

        echo "::: Creating ssh configmap file for user $user with pubkey '$pubkey'"

        # didn't realize k8s accepted application/yaml and ended up doing a lot of conversion lol
        # switched over to yaml in the golang webserver
        cat - << EOT | curl -s -H "Content-Type: application/json" -X "POST" -d @- $k8s_configmap_url
{
    "apiVersion": "v1",
    "kind": "ConfigMap",
    "metadata": {
        "name": "ssh-config-$user",
        "namespace": "$namespace"
    },
    "data": {
        "auth": "$pubkey\n"
    }
}

EOT

        echo "::: Creating ssh pod for user $user with pubkey '$pubkey'"
        cat - << EOT | curl -s -H "Content-Type: application/json" -X "POST" -d @- $k8s_deployment_url
{
    "apiVersion": "apps/v1",
    "kind": "Deployment",
    "metadata": {
        "name": "$user",
        "namespace": "$namespace"
    },
    "spec": {
        "selector": {
            "matchLabels": {
                "app": "ssh-$user"
            }
        },
        "template": {
            "metadata": {
                "labels": {
                    "app": "ssh-$user"
                }
            },
            "spec": {
                "containers": [
                    {
                        "image": "registry.gitlab.com/christian-ondaatje/public/k8s-controller-demo/ssh-box:latest",
                        "imagePullPolicy": "Always",
                        "name": "pytorch-ssh",
                        "ports": [
                            {
                                "containerPort": 22
                            }
                        ],
                        "volumeMounts": [
                            {
                                "mountPath": "/home/sshuser/auth",
                                "name": "ssh-config"
                            }
                        ]
                    }
                ],
                "restartPolicy": "Always",
                "volumes": [
                    {
                        "configMap": {
                            "name": "ssh-config-$user"
                        },
                        "name": "ssh-config"
                    }
                ]
            }
        }
    }
}
EOT


        echo "::: Creating ssh NodePort service for user $user"
        cat - << EOT | curl -s -H "Content-Type: application/json" -X "POST" -d @- $k8s_service_url

{
    "apiVersion": "v1",
    "kind": "Service",
    "metadata": {
        "name": "$user-ssh-service",
        "namespace": "ssh-control"
    },
    "spec": {
        "ports": [
            {
                "name": "ssh",
                "port": 2222,
                "protocol": "TCP",
                "nodePort": $nodeport,
                "targetPort": 22
            }
        ],
        "selector": {
            "app": "ssh-$user"
        },
        "type": "NodePort"
    }
}

EOT
    fi

    # # Act only when configmap is modified and an annotation has been given
    # if [ $type = "MODIFIED" ] && [ -n "$pod_selector" ]; then
    #   delete_pods_with_selector "$pod_selector"
    # fi
  done
}

start_event_loop
