// implements a basic webserver that presents an html form
// allowing a user to submit their ssh pubkey

// internally, this creates a configmap with the relevant user's pubkey
// and submits it to the ssh-control namespace via the kubeapi-proxy container that
// is running alongside this webserver

package main

import (
	"fmt"
	"io"
	"net/http"
	"strings"
	"text/template"
)

// keep track of usernames so we can give them a unique ssh box
var userSet = make(map[string]int)
var nodeportIterator = 30100
var configmapURL = "http://localhost:8001/api/v1/namespaces/ssh-control/configmaps"

const configmapString = `---
apiVersion: v1
kind: ConfigMap
metadata:
  name: %s
data:
  user: %s 
  nodeport: "%d"
  pubkey: %s
`

const index = `
<html>
  <head>
    <link rel="icon" type="image/svg+xml" href="https://christian.ondaatje.org/images/favicon.svg">
    <title>Demo Cloud</title>
    <style>
      h1, h2, h3 {
        text-align: center;
        color: #f8f299;
        background-color: #272822;
      }
      body {
        font-family: "Fira Code", "DejaVu Sans Mono", "Lucida Console", monospace;
        background-color: #f8f8f2;
        color: #665c54;
      }
      code {
        color: #f8f8f2;
        background-color: #665c54;
      }
      form {
        display: block;
        margin: 0 auto;
        max-width: 500px;
        padding: 1em;
        border: 2px solid #222;
        border-radius: 10px;
        box-shadow: 0 4px 8px 0 rgb(0,0,0);
      }
      label {
        display: block;
        margin-bottom: 0.5em;
      }
      input,
      textarea {
        display: block;
        width: 100%;
        padding: 0.5em;
        font-size: 1.2em;
        margin-bottom: 1em;
      }
      input[type="submit"] {
        background-color: #4CAF50;
        color: white;
        border: none;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        cursor: pointer; /* mouse over gives link cursor */
        margin: 4px 2px;
      }
      input[type="submit-delete"] {
        background-color: #f44336;
        color: red;
        border: none;
        padding: 5px 3px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 8px;
        margin: 1px 2px;
        cursor: pointer; /* mouse over gives link cursor */
      }
      .inline-code {
        font-family: monospace;
        font-size: 0.9em;
        color: #444;
        background-color: #ccc;
        padding: 2px;
        border-radius: 3px;
        overflow: auto;
        white-space: pre-wrap;
        word-wrap: break-word;
      }
    </style>
  </head>
  <body>
    <h1>SSH Demo</h1>
    <form action="/" method="post">
      <label for="user">User:</label>
      <input type="text" id="user" name="user"><br>
      <label for="pubkey">Public Key:</label>
      <input type="text" id="pubkey" name="pubkey" rows="4" cols="50"></textarea><br>
      <input type="submit" value="Submit">
    </form>
	<br/>
	<br/>
    {{if gt (len .SSHBoxes) 0}}
      <h2>Existing SSH Boxes</h2>
      <table style="margin: auto; padding: 1em;">
        <tr>
          <th style="padding: 0.5em 1em;">User</th>
          <th style="padding: 0.5em 1em;">Command</th>
          <!-- <th style="padding: 0.5em 1em;">Delete</th> -->
        </tr>
        <hr style="margin: 0.5em 0;" />
        {{range $user, $port := .SSHBoxes}}
          <tr>
            <td style="padding: 0.5em 1em;">{{$user}}</td>
            <td style="padding: 0.5em 1em;"><code class="inline-code" style="text-align: center;">ssh sshuser@sshbox.squire.exchange -p {{$port}}</code></td>
            <!-- <td>
              <form action="/remove/{{$user}}" method="post">
                <input type="asdf" value="Remove">
              </form>
            </td> -->
          </tr>
        {{end}}
      </table>
    {{end}}
  </body>
</html
`

func main() {
	println("starting webserver on port 8099")
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8099", nil)
}

func handler(w http.ResponseWriter, r *http.Request) {
	// we only handle GET or POST requests
	if r.Method != "GET" && r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		io.WriteString(w, "only GET or POST requests supported")
		return
	}

	if r.URL.Path != "/" {
		w.WriteHeader(http.StatusNotFound)
		io.WriteString(w, "404 not found")
		return
	}

	if r.Method == "GET" {
		writeGetResponse(w)
	}
	if r.Method == "POST" {
		writePostResponse(w, r)
	}
}

func writeGetResponse(w http.ResponseWriter) {
	// render and serve the html form
	templatedIndex := template.Must(template.New("index").Parse(index))
	err := templatedIndex.Execute(w, map[string]interface{}{
		"SSHBoxes": userSet})

	if err != nil {
		println(err)
	}
	return
}

func writePostResponse(w http.ResponseWriter, r *http.Request) {
	// read the submitted form
	r.ParseForm()
	user := r.Form.Get("user")

	// check if the user is in the userset
	if _, ok := userSet[user]; ok {
		existingNodeport := userSet[user]

		io.WriteString(w, fmt.Sprintf("ERROR: user already has an ssh box at port %d", existingNodeport))
		return
	}

	pubkey := r.Form.Get("pubkey")
	configname := fmt.Sprintf("ssh-box-config-%s-%d", user, nodeportIterator)
	// write the user's pubkey to a configmap
	sshConfig := strings.TrimSpace(
		fmt.Sprintf(configmapString,
			configname,
			user,
			nodeportIterator,
			pubkey),
	)

	submit(sshConfig)

	userSet[user] = nodeportIterator
	nodeportIterator += 1

	writeGetResponse(w)
	return
}

func submit(sshConfig string) {
	// submit the configmap to the kubernetes api
	println(sshConfig)

	r, err := http.Post(
		configmapURL,
		"application/yaml",
		strings.NewReader(sshConfig),
	)

	println(r, err)
}
